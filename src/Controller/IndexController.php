<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index.html.twig' );
    }

    /**
     * @Route("/zamowienie", name="zamowienie")
     */
    public function zamowienie()
    {
        foreach($_GET as $get)
        {
            dump($get);
        }
    }
}
